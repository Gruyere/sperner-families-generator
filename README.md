# Sperner families generator

The goal of this code is to generate all inequivalent Sperner families over a
set of $`n`$ base elements, for $`1 \leq n \leq 7`$.  Some definitions to understand what
this means:

  - Let $`E`$ be a set. A *Sperner family* (SPF) over $`E`$ is a set of subsets of $`E`$ such
      that any two sets in the SPF are incomparable by inclusion. See
      [https://en.wikipedia.org/wiki/Sperner_family](https://en.wikipedia.org/wiki/Sperner_family). For instance, if $`E = \{A,B,C,D\}`$,
      then $`\{\{A,B,C\},\{B,D\},\{A,D\}\}`$ is an SPF.

  - Two SPFs $`\mathrm{spf}, \mathrm{spf'}`$ are said to be *equivalent* when they are the same up
      to some [permutation](https://en.wikipedia.org/wiki/Permutation) of the elements of $`E`$.
      For instance with $`E = \{0,1,2\}`$, we have that $`\mathrm{spf} = \{\{0,1\}, \{2\}\}`$ and $`\mathrm{spf'} = \{\{0,2\}, \{1\}\}`$
      are equivalent, since we can obtain $`\mathrm{spf'}`$ from $`\mathrm{spf}`$ with the following permutation: $`0 \mapsto 0, 1 \mapsto 2, 2 \mapsto 1`$.

  - By “generating all inequivalent Sperner families”, I then simply mean
      “computing exactly one representative of each equivalent class”.


From now on we work with $`E = \{0, \ldots, n-1\}`$, because we actually
don't care what the base elements are. We will also use $`k = n-1`$.

The number of inequivalent SPFs for $`n \leq 8`$ is given by the OEIS sequence
[A003182](http://oeis.org/A003182).  
The value for $`n=8`$ is computed in [https://arxiv.org/abs/2108.13997](https://arxiv.org/abs/2108.13997), but without generating the SPFs.

A related sequence is to count the number of (not necessarily inequivalent)
SPFs.  These are called “Dedekin numbers”, see
[https://en.wikipedia.org/wiki/Dedekind_number](https://en.wikipedia.org/wiki/Dedekind_number) and [https://oeis.org/A000372](https://oeis.org/A000372).

I point out here that an SPF can equivalently be seen as a monotone Boolean
function: we can see an SPF as a monotone [DNF](https://en.wikipedia.org/wiki/Disjunctive_normal_form) or a monotone [CNF](https://en.wikipedia.org/wiki/Conjunctive_normal_form).
So this code also generates all inequivalent (up to renaming the variables)
monotone Boolean functions on $`1 \leq  n \leq 7`$ variables.


# Acknowledgments

The code is more or less exactly the C++ version of the Python code by Romain
Cazé to do the same task, available at [https://github.com/rcaze/PlosCB2013](https://github.com/rcaze/PlosCB2013). 


# Requirements

1. A C++ compiler (like g++)
2. Optionally, Python and Bash (used by some small helper scripts)
3. To generate all the SPFs for $`7`$ elements, you will need about TODO RAM.


# How are SPFs represented

In the code, an SPF is represented as a strictly increasing list (to be
precise, a C++ std::vector) of integers. Each integer is between $`1`$ and $`(2^n -1)`$
and represents a subset of $`E`$ as follows: element $`0 \leq i \leq k`$ is in the set
if and only if the $`(i+1)`$-th least significant bit in the binary representation
of the integer is $`1`$. So for instance, for $`k = 2`$, the integer $`6`$ represents the set
$`\{1,2\}`$, because its binary representation is $`110`$. An SPF could then be $`[1,6]`$, thus
representing $`\{\{0\},\{1,2\}\}`$.

We say that an SPF is of level $`l`$ if it contains $`l`$ sets.
We say “the level $`i`$” to mean all SPFs of level $`i`$.


# How to use

1. Compile with `g++ -Wall -fopenmp -DGENERATE -DNUM_THREADS=NB -O3
   generator.cpp`, replace NB by the number of threads you want to allocate. The
   execution time should roughly be inversely proportional to the number of
   threads. RAM used however might increase slightly with number of threads.
2. Use `./a.out k l` to generate the $`l`$-th level for a base set of $`k+1`$ elements, assuming you have already computed
   level $`l-1`$. Or `./run_all_for_k_until_level_l.sh k l` to generate all levels for $`1 \leq i \leq l`$.

The functions of level $`l`$ for $`k+1`$ elements are stored in the file sperner/all_ineq/k/l, one per line.

There is a helper python script count.py to count them:
 - `python count.py sperner/all_ineq/k/` to count all the functions stored in files for $`k`$.
 - `python count.py sperner/all_ineq/k/ l` to count all functions in level $`l`$ for $`k`$.

There is another helper script to list the files that already exist:
 - `./files.sh` lists all the files (note that a file has no reason to exist unless it contains all the functions of the corresponding level).

So if everything goes fine, the values of `python count.py
sperner/all_ineq/k/` for $`k = 1 \ldots 6`$ should correspond to the sequence
[http://oeis.org/A003182](http://oeis.org/A003182) with minus $`2`$ everywhere (because we are not counting
the false and the true Boolean functions).    


# Additional

You can also generate all the inequivalent SPFs that use all the base elements
(we call these *nondegenerate*). This can be useful because an SPF might have
been computed for different values of $`k`$. For instance for $`k=3`$ the SPF
{{1},{0,2}} is degenerate, and it already appears for $`k = 2`$. To do that, 
compile with `g++ -Wall -fopenmp -DFILTER -O3 generator.cpp` (will use only one 
thread but this is fast enough). The nondegenerate SPFs will be stored in files
sperner/all_ineq_nondegenerate/k/l.

