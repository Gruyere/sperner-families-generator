#!/bin/bash

printf "========== ALL_INEQ\n"
k=1
while [ -d "sperner/all_ineq/$k" ]; do
  printf "In sperner/all_ineq/$k:" 
  l=1  
  while [ -f "sperner/all_ineq/$k/$l" ]; do
    printf " $l"
    l=$((l + 1))
  done
  k=$((k + 1))
  printf "\n"
done

printf "\n========== ALL_INEQ_NONDEGENERATE\n"
k=1
while [ -d "sperner/all_ineq_nondegenerate/$k" ]; do
  printf "In sperner/all_ineq_nondegenerate/$k:" 
  l=1  
  while [ -f "sperner/all_ineq_nondegenerate/$k/$l" ]; do
    printf " $l"
    l=$((l + 1))
  done
  k=$((k + 1))
  printf "\n"
done

