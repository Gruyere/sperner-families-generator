#!/usr/bin/env python

import sys
import os.path


'''
  Counts number of functions stored in file filepath (this is just the number of lines).
'''
def count(filepath):
    with open(filepath) as f:
        i=-1
        for i, l in enumerate(f):
            pass
    return i + 1



if __name__ == '__main__':
    
    dirname = sys.argv[1]
    
    if dirname[-1] != '/':
        print("Usage:\n \"python count.py dirname/ level\" to count all functions stored in file dirname/level, and\n \"python count.py dirname/\" to count all functions stored in files in dirname.")
        sys.exit()
    
    if not os.path.exists(dirname):
        print('The directory ' + dirname + 'does not exist')
        sys.exit()
    
    if len(sys.argv) == 3:
        file_int = int(sys.argv[2])
        print('Counts the number of functions stored in file ' + dirname + str(file_int))
        if not os.path.isfile(dirname +  str(file_int)):
            print('This file does not exist')
            sys.exit()
        print('Counted ' + str(count(dirname + str(file_int))))
    
    else:
        print('Counts the number of functions stored in the files of directory ' + dirname)
        total = 0
        l = 1
        while os.path.isfile(dirname + str(l)):
            total += count(dirname + str(l))
            l += 1
        print('Counted ' + str(total))
        print('BEWARE: add 2 if you want to count the 2 functions true and false.')
            
    
    
    
